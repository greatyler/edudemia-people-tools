<?php

function edudms_pt_people_cycle($member_type = 'faculty', $sort_by = 'first_name') {

	

	$args = array(
		'blog_id'      => $GLOBALS['blog_id'],
		'role'         => '',
		'meta_key'     => 'edudms_pt_member_type',
		'meta_value'   => $member_type,
		'meta_compare' => '',
		'meta_query'   => array(),
		'date_query'   => array(),        
		'include'      => array(),
		'exclude'      => array(),
		'offset'       => '',
		'search'       => '',
		'number'       => '',
		'count_total'  => false,
		'fields'       => 'all',
		'who'          => '',
	); 

$edudms_pt_cycle = get_users( $args );
	
	
	usort($edudms_pt_cycle, create_function('$a, $b', 'return strnatcasecmp($a->' . $sort_by . ', $b->' . $sort_by . ');'));	

	return $edudms_pt_cycle;

	
}


function edudms_pt_header_output($headers) {
	edudms_pt_headers_start();
	$headers_array = comma_delimited_parser($headers);
	foreach($headers_array as $header) {
		echo '<div class="' . $header . ' edudms_pt_pp_header">' . $header . '</div>';
	}
	edudms_pt_headers_end();
}

function edudms_pt_header_creator($headers) {
	
}

function comma_delimited_parser($string) {
	$clean_string = preg_replace( '/, /', ',', $string);
	$new_array = explode ( ',' , $clean_string);
	return $new_array;
}

function edudms_pt_add_header($field_slug, $header_text, $order, $header_set = 'main') {
	
}

function edudms_pt_headers_start() {
	echo '<div class="pp-header-block"> <!--Start Header Block-->';
}

function edudms_pt_headers_end() {
	echo '</div> <!--End Header Block-->';
}


function edudms_pt_list_person_output($user_identifier, $fields) {
	$fields_array = comma_delimited_parser($fields);

	foreach($fields_array as $field) {
	
	$tester = get_user_meta($user_identifier, $field);
		
		echo '<div class="show_it prop-2 ';
		echo $field;
		echo '">';
		echo $tester[0];
		echo '</div>';
	}

}








?>