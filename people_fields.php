<?php


include_once dirname(__FILE__) . '/member_types.php';



//Add User-People Profile Fields


add_action( 'show_user_profile', 'show_tabbed_profile_fields' );
add_action( 'edit_user_profile', 'show_tabbed_profile_fields' );
 
function show_tabbed_profile_fields( $user ) { 


$edudms_pt_customtab1_name = get_option('edudms_pt_customtab1_name_field_setting');
$edudms_pt_customtab2_name = get_option('edudms_pt_customtab2_name_field_setting');
$edudms_pt_customshortfield1_name = get_option('edudms_pt_customshortfield1_name_field_setting');
$edudms_pt_customshortfield2_name = get_option('edudms_pt_customshortfield2_name_field_setting');

?>

    <h3>Faculty Profile Page Information</h3>
    <table class="form-table">
		<?php member_type_field() ?>
		<?php if ( get_option( 'edudms_pt_customshortfield1_field_setting' ) == 1 ) { ?>
		<tr>
            <th><label><?php echo $edudms_pt_customshortfield1_name; ?></label></th>
             <td>
                <input type="text" id="edudms_pt_customshortfield1" name="edudms_pt_customshortfield1" size="40" value="<?php echo get_the_author_meta( 'edudms_pt_customshortfield1', $user->ID ); ?>">
                <span class="description">Please create your <?php echo $edudms_pt_customshortfield1_name ?> Section.</span>
            </td>
        </tr>
		<?php } ?>
		<?php if ( get_option( 'edudms_pt_customshortfield2_field_setting' ) == 1 ) { ?>
		<tr>
            <th><label><?php echo $edudms_pt_customshortfield2_name; ?></label></th>
             <td>
				<input type="text" id="edudms_pt_customshortfield2" name="edudms_pt_customshortfield2" size="40">
                <span class="description">Please fill in <?php echo $edudms_pt_customshortfield2_name ?>.</span>
            </td>
        </tr>
		<?php } ?>
        <?php if ( get_option( 'edudms_pt_bio_field_setting' ) == 1 ) { ?>
		<tr>
			<th><label>Bio</label></th>
             <td>
				<?php $edudms_pt_bio = get_the_author_meta( 'edudms_pt_bio', $user->ID );
				wp_editor( $edudms_pt_bio, 'edudms_pt_bio' ); ?>
                <br />
                <span class="description">Please create your Bio Section.</span>
            </td>
        </tr> 
		 <?php } ?>
		 <?php if ( get_option( 'edudms_pt_cv_field_setting' ) == 1 ) { ?>
		<tr>
            <th><label>CV</label></th>
             <td>
                <?php $edudms_pt_cv = get_the_author_meta( 'edudms_pt_cv', $user->ID );
				wp_editor( $edudms_pt_cv, 'edudms_pt_cv' ); ?><br />
                <span class="description">Please create your CV Section.</span>
            </td>
        </tr>
		<tr>
		<?php } ?>
		<?php if ( get_option( 'edudms_pt_courses_field_setting' ) == 1 ) { ?>
		<tr>
            <th><label>Courses</label></th>
             <td>
                <?php $edudms_pt_courses = get_the_author_meta( 'edudms_pt_courses', $user->ID );
				wp_editor( $edudms_pt_courses, 'edudms_pt_courses' ); ?><br />
                <span class="description">Please create your Courses Section.</span>
            </td>
        </tr>
		<?php } ?>
		<?php if ( get_option( 'edudms_pt_publications_field_setting' ) == 1 ) { ?>
		<tr>
            <th><label>Publications</label></th>
             <td>
                <?php $edudms_pt_publications = get_the_author_meta( 'edudms_pt_publications', $user->ID );
				wp_editor( $edudms_pt_publications, 'edudms_pt_publications' ); ?><br />
                <span class="description">Please create your Publications Section.</span>
            </td>
        </tr>
		<?php } ?>
		<?php if ( get_option( 'edudms_pt_research_field_setting' ) == 1 ) { ?>
		<tr>
            <th><label>Research</label></th>
             <td>
                <?php $edudms_pt_research = get_the_author_meta( 'edudms_pt_research', $user->ID );
				wp_editor( $edudms_pt_research, 'edudms_pt_research' ); ?><br />
                <span class="description">Please create your Research Section.</span>
            </td>
        </tr>
		<?php } ?>
		<?php if ( get_option( 'edudms_pt_customtab1_field_setting' ) == 1 ) { ?>
		<tr>
            <th><label><?php echo $edudms_pt_customtab1_name; ?></label></th>
             <td>
                <?php $edudms_pt_customtab1 = get_the_author_meta( 'edudms_pt_customtab1', $user->ID );
				wp_editor( $edudms_pt_customtab1, 'edudms_pt_customtab1' ); ?><br />
                <span class="description">Please create your <?php echo $edudms_pt_customtab1_name ?> Section.</span>
            </td>
        </tr>
		<?php } ?>
		<?php if ( get_option( 'edudms_pt_customtab2_field_setting' ) == 1 ) { ?>
		<tr>
            <th><label><?php echo $edudms_pt_customtab2_name; ?></label></th>
             <td>
                <?php $edudms_pt_customtab1 = get_the_author_meta( 'edudms_pt_customtab2', $user->ID );
				wp_editor( $edudms_pt_customtab2, 'edudms_pt_customtab2' ); ?><br />
                <span class="description">Please create your <?php echo $edudms_pt_customtab2_name ?> Section.</span>
            </td>
        </tr>
		<?php } ?>
		
    </table>
	
	
<?php }





function validate_wfu_phone () {
	//preg_replace(^1
}






 
function my_save_extra_profile_fields( $user_id ) {
 
    if ( !current_user_can( 'edit_user', $user_id ) )
        return false;
 
	$customtab1_metaname = (string)$customtab1_metaname;
	$customshortfield1_metaname = (string)$customshortfield1_metaname;
    update_user_meta( absint( $user_id ), 'edudms_pt_bio', wp_kses_post( $_POST['edudms_pt_bio'] ) );
	update_user_meta( absint( $user_id ), 'edudms_pt_cv', wp_kses_post( $_POST['edudms_pt_cv'] ) );
	update_user_meta( absint( $user_id ), 'edudms_pt_courses', wp_kses_post( $_POST['edudms_pt_courses'] ) );
	update_user_meta( absint( $user_id ), 'edudms_pt_publications', wp_kses_post( $_POST['edudms_pt_publications'] ) );
	update_user_meta( absint( $user_id ), 'edudms_pt_research', wp_kses_post( $_POST['edudms_pt_research'] ) );
	update_user_meta( absint( $user_id ), 'edudms_pt_customtab1', wp_kses_post( $_POST['edudms_pt_customtab1'] ) );
	update_user_meta( absint( $user_id ), 'edudms_pt_member_type', wp_kses_post( $_POST['edudms_pt_member_type'] ) );
	update_user_meta( absint( $user_id ), 'edudms_pt_customtab2', wp_kses_post( $_POST['edudms_pt_customtab2'] ) );
	update_user_meta( absint( $user_id ), 'edudms_pt_customshortfield1', wp_kses_post( $_POST['edudms_pt_customshortfield1'] ) );
	update_user_meta( absint( $user_id ), 'edudms_pt_customshortfield2', wp_kses_post( $_POST['edudms_pt_customshortfield2'] ) );
}

add_action( 'personal_options_update', 'my_save_extra_profile_fields' );
add_action( 'edit_user_profile_update', 'my_save_extra_profile_fields' );


function edudms_pt_add_contact_fields( $contact_methods ) {
	$contact_methods['edudms_pt_title'] = 'Title';
	$contact_methods['edudms_pt_phone'] = 'Phone';
	$contact_methods['edudms_pt_office'] = 'Office Location';
	// $contact_methods['jabber'] = ''; // just removes label, have to find how to remove it.
	return $contact_methods;
};
add_filter( 'user_contactmethods', 'edudms_pt_add_contact_fields' );




//Kill Stuff on Profile Page (editing side)

function hide_personal_options(){
echo "\n" . '<script type="text/javascript">jQuery(document).ready(function($) { $(\'form#your-profile > h3:first\').hide(); $(\'form#your-profile > table:first\').hide(); $(\'form#your-profile\').show(); });</script>' . "\n";
}
add_action('admin_head','hide_personal_options');

//don't kill account management if admin

require_once(ABSPATH . 'wp-includes/pluggable.php');

if(current_user_can('remove_users') !== true) {


function hide_account_management_options(){
echo "\n" . '<script type="text/javascript">jQuery(document).ready(function($) { $(\'tr#password\').hide(); });</script>' . "\n";
}
add_action('admin_head','hide_account_management_options');

add_action( 'personal_options', array ( 'T5_Hide_Profile_Bio_Box', 'start' ) );

}

/**
 * Captures the part with the biobox in an output buffer and removes it.
 *
 * @author Thomas Scholz, <info@toscho.de>
 *
 */
class T5_Hide_Profile_Bio_Box
{
    /**
     * Called on 'personal_options'.
     *
     * @return void
     */
    public static function start()
    {
        $action = ( IS_PROFILE_PAGE ? 'show' : 'edit' ) . '_user_profile';
        add_action( $action, array ( __CLASS__, 'stop' ) );
        ob_start();
    }

    /**
     * Strips the bio box from the buffered content.
     *
     * @return void
     */
    public static function stop()
    {
        $html = ob_get_contents();
        ob_end_clean();

        // remove the headline
        $headline = __( IS_PROFILE_PAGE ? 'About Yourself' : 'About the user' );
        $html = str_replace( '<h3>' . $headline . '</h3>', '', $html );

        // remove the table row
        $html = preg_replace( '~<tr class="user-description-wrap">\s*<th><label for="description".*</tr>~imsUu', '', $html );
        print $html;
    }
}

?>