<?php
/*
Plugin Name: Edudemia People Tools
Plugin URI: https://bitbucket.org/greatyler/edudemia-people-tools
Bitbucket Plugin URI: greatyler/edudemia-people-tools
Description: Add Extensive User and People Management Tools for WordPress
Version: 0.7.0
Author: Tyler Pruitt
Author URI: http://tpruitt.capeville.wfunet.wfu.edu/tylerpress/
Text Domain: college-people-tools
License: GPL version 2 or later - http://www.gnu.org/licenses/old-licenses/gpl-2.0.html

Credits:
This plugin's director of development and primary developer is Tyler Pruitt.
This plugin is developed in conjunction with College Web Starter Theme by Robert Vidrine.
Thank You to Jo Lowe, Tommy Murphy, and Robert Vidrine for code contributions,
design consultation, and general sanity checks throughout the development process.

The following plugins were sourced or referrenced in this project:
Code from:



Dependencies or Referrences:





License

College People is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.

College Menus is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
 
See http://www.gnu.org/licenses/old_licenses/gpl_2.0.en.html.
*/



// Base materials


include(dirname(__FILE__) . '/people_fields.php');
include(dirname(__FILE__) . '/misc_tools.php');
include(dirname(__FILE__) . '/people-cycle.php');



function edudms_pt_css_registration() {
wp_register_style('edudms_pt', plugins_url('edudms_pt.css',__FILE__ ));
wp_enqueue_style('edudms_pt');
}
add_action( 'wp_enqueue_scripts','edudms_pt_css_registration');

function edudms_pt_admin_bar_render() {
			global $wp_admin_bar;
			$wp_admin_bar->remove_menu('edit');
}

include(dirname(__FILE__) . '/menu_page.php');


function edudms_pt_enqueue_admin_css() {
wp_register_style( 'edudms_pt_admin_css', plugins_url('css/edudms_pt_admin_css.css',__FILE__ ));
        wp_enqueue_style( 'edudms_pt_admin_css' );
}
add_action( 'admin_enqueue_scripts', 'edudms_pt_enqueue_admin_css' );


function edudms_pt_enqueue_edudms_ptcss_to_admin() {
wp_register_style( 'edudms_pt_css_in_admin', plugins_url('edudms_pt.css',__FILE__ ));
        wp_enqueue_style( 'edudms_pt_css_in_admin' );
}

add_action( 'admin_enqueue_scripts', 'edudms_pt_enqueue_edudms_ptcss_to_admin' );





/*
 * @author    Thomas Griffin, Gary Jones, Juliette Reinders Folmer
 * @copyright Copyright (c) 2011, Thomas Griffin
 * @license   GPL-2.0+
*/

require_once dirname(__FILE__) . '/tgm-plugin/edudms-pt-tgm-plugin-activation.php';

add_action( 'tgmpa_register', 'edudms_pt_register_required_plugins' );


function edudms_pt_register_required_plugins() {
	
	$plugins = array(
	
		array(
				'name'        => 'Tabby Responsive Tabs',
				'slug'        => 'tabby-responsive-tabs',
				'is_callable' => 'tabby_init',
				'force_activation' => true,
			),
		array(
				'name'        => 'WP User Avatar',
				'slug'        => 'wp-user-avatar',
				'is_callable' => 'wp_user_avatar_init',
				'force_activation' => true,
			),	
			
	);
	
	$config = array(
		'id'           => 'edudms_pt_tgmpa',                 // Unique ID for hashing notices for multiple instances of TGMPA.
		'default_path' => '',                      // Default absolute path to bundled plugins.
		'menu'         => 'tgmpa-install-plugins', // Menu slug.
		'parent_slug'  => 'options-general.php',            // Parent menu slug.
		'capability'   => 'edit_theme_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
		'has_notices'  => true,                    // Show admin notices or not.
		'dismissable'  => false,                    // If false, a user cannot dismiss the nag message.
		'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
		'is_automatic' => true,                   // Automatically activate plugins after installation or not.
		'message'      => '',                      // Message to output right before the plugins table.

		
		'strings'      => array(
			'page_title'                      => __( 'Plugins Required for College People Tools', 'theme-slug' ),
			'menu_title'                      => __( 'Install People Tools Plugins', 'theme-slug' ),
			'installing'                      => __( 'Installing Plugin: %s', 'theme-slug' ), // %s = plugin name.
			'oops'                            => __( 'Something went wrong with the plugin API.', 'theme-slug' ),
			'notice_can_install_required'     => _n_noop(
				'College People Tools requires the following plugin: %1$s.',
				'College People Tools requires the following plugins: %1$s.',
				'theme-slug'
			), // %1$s = plugin name(s).
			'notice_can_install_recommended'  => _n_noop(
				'College People Tools recommends the following plugin: %1$s.',
				'College People Tools recommends the following plugins: %1$s.',
				'theme-slug'
			), // %1$s = plugin name(s).
			'notice_cannot_install'           => _n_noop(
				'Sorry, but you do not have the correct permissions to install the %1$s plugin.',
				'Sorry, but you do not have the correct permissions to install the %1$s plugins.',
				'theme-slug'
			), // %1$s = plugin name(s).
			'notice_ask_to_update'            => _n_noop(
				'The following plugin needs to be updated to its latest version to ensure maximum compatibility with College People Tools: %1$s.',
				'The following plugins need to be updated to their latest version to ensure maximum compatibility with College People Tools: %1$s.',
				'theme-slug'
			), // %1$s = plugin name(s).
			'notice_ask_to_update_maybe'      => _n_noop(
				'There is an update available for: %1$s.',
				'There are updates available for the following plugins: %1$s.',
				'theme-slug'
			), // %1$s = plugin name(s).
			'notice_cannot_update'            => _n_noop(
				'Sorry, but you do not have the correct permissions to update the %1$s plugin.',
				'Sorry, but you do not have the correct permissions to update the %1$s plugins.',
				'theme-slug'
			), // %1$s = plugin name(s).
			'notice_can_activate_required'    => _n_noop(
				'The following required plugin is currently inactive: %1$s.',
				'The following required plugins are currently inactive: %1$s.',
				'theme-slug'
			), // %1$s = plugin name(s).
			'notice_can_activate_recommended' => _n_noop(
				'The following recommended plugin is currently inactive: %1$s.',
				'The following recommended plugins are currently inactive: %1$s.',
				'theme-slug'
			), // %1$s = plugin name(s).
			'notice_cannot_activate'          => _n_noop(
				'Sorry, but you do not have the correct permissions to activate the %1$s plugin.',
				'Sorry, but you do not have the correct permissions to activate the %1$s plugins.',
				'theme-slug'
			), // %1$s = plugin name(s).
			'install_link'                    => _n_noop(
				'Begin installing plugin',
				'Begin installing plugins',
				'theme-slug'
			),
			'update_link' 					  => _n_noop(
				'Begin updating plugin',
				'Begin updating plugins',
				'theme-slug'
			),
			'activate_link'                   => _n_noop(
				'Begin activating plugin',
				'Begin activating plugins',
				'theme-slug'
			),
			'return'                          => __( 'Return to Required Plugins Installer', 'theme-slug' ),
			'plugin_activated'                => __( 'Plugin activated successfully.', 'theme-slug' ),
			'activated_successfully'          => __( 'The following plugin was activated successfully:', 'theme-slug' ),
			'plugin_already_active'           => __( 'No action taken. Plugin %1$s was already active.', 'theme-slug' ),  // %1$s = plugin name(s).
			'plugin_needs_higher_version'     => __( 'Plugin not activated. A higher version of %s is needed for College People Tools. Please update the plugin.', 'theme-slug' ),  // %1$s = plugin name(s).
			'complete'                        => __( 'All plugins installed and activated successfully. %1$s', 'theme-slug' ), // %s = dashboard link.
			'contact_admin'                   => __( 'Please contact the administrator of this site for help.', 'tgmpa' ),

			'nag_type'                        => 'updated', // Determines admin notice type - can only be 'updated', 'update-nag' or 'error'.
		),
		
	);

	tgmpa( $plugins, $config );
}

























// Menu Options

function change_users_to_people() {
	global $menu;
	global $submenu;
	$menu[70][0] = 'People';
	$submenu['users.php'][5][0] = 'All People';
    $submenu['users.php'][10][0] = 'Add Person';
}

add_action( 'admin_menu', 'change_users_to_people' );






//Add Main Admin Menu Page
add_action ( 'admin_menu', 'edudms_pt_menu_page');
function edudms_pt_menu_page() {
		$edudms_pt_menu_page = add_menu_page ( 'People Tools', 'People Tools', 'read', 'edudms_ptslug', 'edudms_pt_menu_page_render', plugin_dir_url( __FILE__ ) . 'images/icon_cream.png' );
		add_action( 'load-' . $edudms_pt_menu_page, 'load_edudms_ptcss_in_admin' );
		$edudms_pt_people_settings_subpage = add_users_page ( 'Edudemia People Tools', 'Settings', 'remove_users', 'edudms_pt_people_settings_subpage_slug', 'edudms_pt_menu_page_render' );
	}
	function load_edudms_ptcss_in_admin(){
			// Unfortunately we can't just enqueue our scripts here - it's too early. So register against the proper action hook to do it
			add_action( 'admin_enqueue_styles', 'enqueue_edudms_ptcss_in_admin' );
		}
	function enqueue_edudms_ptcss_in_admin(){
			wp_enqueue_script( 'edudms_pt_css_in_admin', plugins_url('edudms_pt.css',__FILE__ ) );
		}











// Plugin Activation Functions





// Plugin Deactivation



function edudms_pt_deactivation() {
 
    // Our post type will be automatically removed, so no need to unregister it
 
    // Clear the permalinks to remove our post type's rules
    flush_rewrite_rules();
 
}

register_deactivation_hook( __FILE__, 'edudms_pt_deactivation' );


// Shortcodes


add_shortcode ('edudms_pt', 'edudms_pt_show');
add_shortcode ('pt_list', 'edudms_pt_list_start');
add_shortcode ('end_pt_list', 'edudms_pt_list_end');




?>