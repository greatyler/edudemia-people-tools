<?php


function term_it ( $term, $term_description ) {
		echo '<div class="edudms_pt_term_single_wrapper">';
		echo '<div class="edudms_pt_term">' . $term . '</div>';
		echo '<div class="edudms_pt_term_description">' . $term_description . '</div> <!-- End Term Content -->';
		echo '</div> <!-- End Term Single Wrapper -->';
}















function edudms_pt_help_render () {
	
	?>
	<table>
			<tr>
				<td>
					<img src="<?php echo plugin_dir_url( __FILE__ ) . 'images/icon32.png'; ?>" />
				</td>
				<td>
					<h1>College People Tools Help Page</h1>
				</td>
			</tr>
		</table>
	
	<div class="edudms_pt_terminology_wrapper">
		<div class="edudms_pt_terminology_title">College People Tools Terminology</div>
		<div class="edudms_pt_terminology_content">
				<div class="edudms_pt_term_single_wrapper">
					<div class="edudms_pt_term">Magic Pages</div>
						<div class="edudms_pt_term_description">
							Of course pages aren't magical. In this sense, Magic refers to the process by which we use the wordpress database to store all the information from everyone's editing Areas and then when a front end user wants to see the information, Magic (lots of code) takes the information out of the database and uses one of our "Magic Pages" to create the Page on the fly. So a "Magic Page" does not exist in the traditional sense, but is recreated every time someone tries to look at it, and it might be recreated with different information. This is why you cannot edit a "Magic Page". On a "Magic Page" the edit page link in the admin bar will disappaer because it is not possible to edit a page that does not exist as a "page".
						</div> <!-- End Term Content -->
					</div> <!-- End Term Single Wrapper -->
				<div class="edudms_pt_term_single_wrapper">
					<div class="edudms_pt_term">Profile Page</div>
						<div class="edudms_pt_term_description">
							A "Magic Page" that takes information from everyone's Editing Areas and arranges it into a well formatted page.
						</div> <!-- End Term Content -->
					</div> <!-- End Term Single Wrapper -->
				<div class="edudms_pt_term_single_wrapper">
					<div class="edudms_pt_term">People Page</div>
						<div class="edudms_pt_term_description">
							A "Magic Page" that takes information from everyone's Editing Areas and arranges it into a well formatted page.
						</div> <!-- End Term Content -->
					</div> <!-- End Term Single Wrapper -->
				<div class="edudms_pt_term_single_wrapper">
					<div class="edudms_pt_term">Custom Short Field</div>
						<div class="edudms_pt_term_description">
							A "Magic Page" that takes information from everyone's Editing Areas and arranges it into a well formatted page.
						</div> <!-- End Term Content -->
					</div> <!-- End Term Single Wrapper -->
			</div>
		</div>
		
		
		
		
	<div class="edudms_pt_howto_wrapper">
		<div class="edudms_pt_howto_title">College People Tools Terminology</div>
		<div class="edudms_pt_howto_content">
				<div class="edudms_pt_howto_single_wrapper">
					<div class="edudms_pt_howto">Create a New Faculty or Staff Member</div>
						<div class="edudms_pt_howto_description">
							Instructions.
						</div> <!-- End Term Content -->
					</div> <!-- End Term Single Wrapper -->
			</div>
		</div>
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	<div class="edudms_pt_about_wrapper">
		<div class="edudms_pt_about_title">About College People Tools</div>
		<div class="edudms_pt_about_content">
				This is all you need to know about College People Tools.
			</div>
		</div>
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
<?php	
} ?>