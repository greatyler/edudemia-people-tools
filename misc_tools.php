<?php



//shortcode function

function edudms_pt_show($atts) {
	
	$shortcode_atts = shortcode_atts( array(
        'member_type' => 'faculty',
		'sort_by' => 'last_name',
		'meta_key' => 'member_type',
		'meta_value' => '',
		'format' => 'tile',
		'fields' => 'last_name, edudms_pt_title',
		'headers' => 'Name,Title'
    ), $atts );
	
	$edudms_pt_cycle = edudms_pt_people_cycle($shortcode_atts['member_type'], $shortcode_atts['sort_by'] );
	if ($shortcode_atts['format'] == 'tile') {
	?> <div class="edudms_tiles_wrapper"> <?php
	foreach ( $edudms_pt_cycle as $user ) {
		$user_identifier = $user->id;
		$profile_template_page = get_option('edudms_pt_profile_page_selection');
		$profile_link = get_permalink( $profile_template_page ) . '?user=' . $user_identifier;
		$profile_blob = '<a href="' . $profile_link .  '">';
		$profile_blob2 = esc_html($profile_blob);
		$first_name = $user->first_name;
		$last_name = $user->last_name;
		$title = $user->edudms_pt_title;
		$email = $user->user_email;
		$website = $user->user_url;
		$phone = $user->edudms_pt_phone;
		$office = $user->edudms_pt_office;
		$edudms_pt_bio = $user->edudms_pt_bio;
		$edudms_pt_cv = $user->edudms_pt_cv;
		$edudms_pt_courses = $user->edudms_pt_courses;
		$full_name = $first_name . ' ' . $last_name;
		$comma_name = $last_name . ', ' . $first_name;
		$edudms_pt_customshortfield1 = $user->edudms_pt_customshortfield1;
		$edudms_pt_customshortfield2 = $user->edudms_pt_customshortfield2;
	
	
	
		?>
		<div class="edudms_tile_wrapper">
				<div class="edudms_tile_title">
						<?php echo $full_name; ?>
					</div> <!-- End edudms_tile_title -->
				<div class="edudms_tile_picture">
						<?php echo 'PIC HERE'; ?>
					</div><!-- End edudms_tile_picture -->
				<div class="edudms_tile_info_wrapper">
						<?php echo 'info fields here'; ?>
					</div><!-- End edudms_tile_info_wrapper -->
			</div> <!-- End edudms_tile_wrapper -->
	<?php }
	?> </div> <!-- End edudms_tiles_wrapper --> <?php
	}
	
	
	if ($shortcode_atts['format'] == 'list') {
		
		
		edudms_pt_header_output($shortcode_atts['headers']);
		foreach ( $edudms_pt_cycle as $user ) {
		$user_identifier = $user->id;
		echo '<div class="person-block"> <!--Start Person Block ' . $user_identifier . '-->';
		edudms_pt_list_person_output($user_identifier, $shortcode_atts['fields']);
		echo '</div> <!--End Person Block ' . $GLOBALS["user_identifier"] . '-->';
	
	
		}

	
	}

}



















?>